package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	private final double batteryLevel;
	
	public NotEnoughBatteryException(final double remainingBattery) {
		super();
		this.batteryLevel = remainingBattery;
	}
	
	public String getMessage() {
		return "Can't move, low battery: " + this.batteryLevel;
	}
}
