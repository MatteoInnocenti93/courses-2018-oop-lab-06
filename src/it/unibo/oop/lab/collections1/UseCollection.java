package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {
	
	private static final int LOWER_BOUND = 1000;
	private static final int UPPER_BOUND = 2000;
	private static final int ELEMENTS_TO_ADD = 100000;
	private static final int FROM_NANO_TO_MS = 1000000;
	private static final int TIMES_TO_READ = 1000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void addElements(final List<Integer> list, final int elementsToAdd) {
    	for(int i = LOWER_BOUND; i < UPPER_BOUND; i++) {
    		list.add(i);
    	}
    }
    
    public static String toString(final List<Integer> list) {
    	String arrayElements = "";
    	for(final Integer elem : list) {
    		arrayElements = arrayElements + "\n" + elem;
    	}
    	return arrayElements;
    }
    
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	final List<Integer> arrayList = new ArrayList<>();
    	addElements(arrayList, 1000);
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final List<Integer> linkedList = new LinkedList<>();
    	linkedList.addAll(arrayList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int supportVariable = arrayList.get(arrayList.size() - 1);
    	arrayList.set(arrayList.size() - 1, arrayList.get(0));
    	arrayList.set(0, supportVariable);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	System.out.println(toString(arrayList));
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	for(int i = 0; i < ELEMENTS_TO_ADD; i++) {
    		arrayList.add(0, i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("\nTime for adding elements in ArrayList: " + time / FROM_NANO_TO_MS + " ms");
    	
    	time = System.nanoTime();
    	for(int i = 0; i < ELEMENTS_TO_ADD; i++) {
    		linkedList.add(0, i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Time for adding elements in LinkedList: " + time / FROM_NANO_TO_MS + " ms");
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	time = System.nanoTime();
    	for(int i = 0; i < TIMES_TO_READ; i++) {
    		arrayList.get(arrayList.size() / 2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("\nTime for reading element in middle of ArrayList: " + time / FROM_NANO_TO_MS + " ms");
    	
    	time = System.nanoTime();
    	for(int i = 0; i < TIMES_TO_READ; i++) {
    		linkedList.get(linkedList.size() / 2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Time for reading element in middle of LinkedList: " + time / FROM_NANO_TO_MS + " ms");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	final Map<String, Long> populations = new HashMap<String, Long>();
    	populations.put("Africa", 1110635000L);
    	populations.put("Americas", 972000000L);
    	populations.put("Antarctica", 0L);
    	populations.put("Asia", 4298723000L);
    	populations.put("Europe", 742452000L);
    	populations.put("Oceania", 38304000L);
        /*
         * 8) Compute the population of the world
         */
    	long totalPopulation = 0;
    	for(String elem : populations.keySet()) {
    		totalPopulation = totalPopulation + populations.get(elem);
    	}
    	System.out.println("\nTotal world population is: " + totalPopulation);
    }
}
